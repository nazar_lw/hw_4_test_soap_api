package requestgenerator;

import model.book.Book;

import javax.xml.soap.*;

import static utils.Constants.*;

public class BookRequestGenerator extends BaseRequestGenerator{

    public SOAPMessage saveBook(Book book, int authorId, int genreId) throws Exception {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();

        envelope.addNamespaceDeclaration(PREFIX, SERVER_URI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement createBookRequestTag =
                soapBody.addChildElement("createBookRequest", PREFIX);

        SOAPElement authorIdTag = createBookRequestTag.addChildElement("authorId", PREFIX);
        authorIdTag.addTextNode(String.valueOf(authorId));
        SOAPElement genreIdTag = createBookRequestTag.addChildElement("genreId", PREFIX);
        genreIdTag.addTextNode(String.valueOf(genreId));

        SOAPElement bookTag = createBookRequestTag.addChildElement("book", PREFIX);
        SOAPElement bookIdTag = bookTag.addChildElement("bookId", PREFIX);
        bookIdTag.addTextNode(String.valueOf(book.getBookId()));
        SOAPElement bookNameTag = bookTag.addChildElement("bookName", PREFIX);
        bookNameTag.addTextNode(book.getBookName());
        SOAPElement bookLanguageTag = bookTag.addChildElement("bookLanguage", PREFIX);
        bookLanguageTag.addTextNode(book.getBookLanguage());
        SOAPElement bookDescriptionTag = bookTag.addChildElement("bookDescription", PREFIX);
        bookDescriptionTag.addTextNode(book.getBookDescription());

        SOAPElement additionalTag = bookTag.addChildElement("additional", PREFIX);
        SOAPElement pageCountTag = additionalTag.addChildElement("pageCount", PREFIX);
        pageCountTag.addTextNode(String.valueOf(book.getAdditional().getPageCount()));

        SOAPElement sizeTag = additionalTag.addChildElement("size", PREFIX);
        SOAPElement heightTag = sizeTag.addChildElement("height", PREFIX);
        heightTag.addTextNode(String.valueOf(book.getAdditional().getSize().getHeight()));
        SOAPElement widthTag = sizeTag.addChildElement("width", PREFIX);
        widthTag.addTextNode(String.valueOf(book.getAdditional().getSize().getWidth()));
        SOAPElement lengthTag = sizeTag.addChildElement("length", PREFIX);
        lengthTag.addTextNode(String.valueOf(book.getAdditional().getSize().getLength()));

        SOAPElement publicationYearTag = bookTag.addChildElement("publicationYear", PREFIX);
        publicationYearTag.addTextNode(String.valueOf(book.getPublicationYear()));

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader(HEADER_NAME, SERVER_URI);
        soapMessage.saveChanges();
        return soapMessage;
    }

    public SOAPMessage getAllBooksByGenre(int genreId) throws SOAPException {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();

        envelope.addNamespaceDeclaration(PREFIX, SERVER_URI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement requestTag =
                soapBody.addChildElement("getBooksByGenreRequest", PREFIX);
        SOAPElement genreIdTag = requestTag.addChildElement("genreId", PREFIX);
        genreIdTag.addTextNode(String.valueOf(genreId));
        SOAPElement searchTag = requestTag.addChildElement("search", PREFIX);
        SOAPElement orderTypeTag = searchTag.addChildElement("orderType", PREFIX);
        orderTypeTag.addTextNode("asc");
        SOAPElement pageTag = searchTag.addChildElement("page", PREFIX);
        pageTag.addTextNode("1");
        SOAPElement paginationTag = searchTag.addChildElement("pagination", PREFIX);
        paginationTag.addTextNode("true");
        SOAPElement sizeTag = searchTag.addChildElement("size", PREFIX);
        sizeTag.addTextNode(INITIAL_SIZE_OF_COLLECTION);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader(HEADER_NAME, SERVER_URI);
        soapMessage.saveChanges();
        return soapMessage;
    }

}
