package requestgenerator;

import model.genre.Genre;

import javax.xml.soap.*;

import static utils.Constants.*;

public class GenreRequestGenerator extends BaseRequestGenerator{

    public GenreRequestGenerator() {
    }

    public SOAPMessage saveGenre(Genre genre) throws Exception {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();

        envelope.addNamespaceDeclaration(PREFIX, SERVER_URI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement createGenreRequestTag =
                soapBody.addChildElement("createGenreRequest", PREFIX);
        SOAPElement genreTag = createGenreRequestTag.addChildElement("genre", PREFIX);
        SOAPElement genreIdTag = genreTag.addChildElement("genreId", PREFIX);
        genreIdTag.addTextNode(String.valueOf(genre.getGenreId()));
        SOAPElement genreNameTag = genreTag.addChildElement("genreName", PREFIX);
        genreNameTag.addTextNode(genre.getGenreName());
        SOAPElement genreDescriptionTag = genreTag.addChildElement("genreDescription", PREFIX);
        genreDescriptionTag.addTextNode(genre.getGenreDescription());

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader(HEADER_NAME, SERVER_URI);
        soapMessage.saveChanges();
        return soapMessage;
    }

}
