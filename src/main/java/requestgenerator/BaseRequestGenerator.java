package requestgenerator;

import javax.xml.soap.*;

import static utils.Constants.*;
import static utils.Constants.SERVER_URI;

public class BaseRequestGenerator {

    public BaseRequestGenerator() {
    }

    public SOAPMessage getSingleEntity(String entityRequestTag, String entityIdTag, int entityId) throws Exception {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();

        envelope.addNamespaceDeclaration(PREFIX, SERVER_URI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement requestTag =
                soapBody.addChildElement(entityRequestTag, PREFIX);
        SOAPElement idTag = requestTag.addChildElement(entityIdTag, PREFIX);
        idTag.addTextNode(String.valueOf(entityId));
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader(HEADER_NAME, SERVER_URI);
        soapMessage.saveChanges();
        return soapMessage;
    }

    public SOAPMessage getAllByOrder(String order, String tagName) throws SOAPException {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();

        envelope.addNamespaceDeclaration(PREFIX, SERVER_URI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement requestTag =
                soapBody.addChildElement(tagName, PREFIX);
        SOAPElement searchTag = requestTag.addChildElement("search", PREFIX);
        SOAPElement orderTypeTag = searchTag.addChildElement("orderType", PREFIX);
        orderTypeTag.addTextNode(order);
        SOAPElement pageTag = searchTag.addChildElement("page", PREFIX);
        pageTag.addTextNode("1");
        SOAPElement paginationTag = searchTag.addChildElement("pagination", PREFIX);
        paginationTag.addTextNode("true");
        SOAPElement sizeTag = searchTag.addChildElement("size", PREFIX);
        sizeTag.addTextNode(INITIAL_SIZE_OF_COLLECTION);

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader(HEADER_NAME, SERVER_URI);
        soapMessage.saveChanges();
        return soapMessage;
    }

    public SOAPMessage deleteEntity(String entityId, String tagName, String entityIdName) throws SOAPException {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();

        envelope.addNamespaceDeclaration(PREFIX, SERVER_URI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement deleteTag =
                soapBody.addChildElement(tagName, PREFIX);
        SOAPElement idTag = deleteTag.addChildElement(entityIdName, PREFIX);
        idTag.addTextNode(entityId);
        SOAPElement optionsTag = deleteTag.addChildElement("options", PREFIX);
        SOAPElement forciblyTag = optionsTag.addChildElement("forcibly", PREFIX);
        forciblyTag.addTextNode("false");
        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader(HEADER_NAME, SERVER_URI);
        soapMessage.saveChanges();
        return soapMessage;
    }

}
