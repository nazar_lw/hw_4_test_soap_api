package requestgenerator;

import model.author.Author;

import javax.xml.soap.*;

import static utils.Constants.*;

public class AuthorRequestGenerator extends BaseRequestGenerator{

    public AuthorRequestGenerator() {
    }

    public SOAPMessage saveAuthor(Author author) throws Exception {

        MessageFactory messageFactory = MessageFactory.newInstance();
        SOAPMessage soapMessage = messageFactory.createMessage();
        SOAPPart soapPart = soapMessage.getSOAPPart();
        SOAPEnvelope envelope = soapPart.getEnvelope();

        envelope.addNamespaceDeclaration(PREFIX, SERVER_URI);
        SOAPBody soapBody = envelope.getBody();
        SOAPElement createAuthorRequestTag =
                soapBody.addChildElement("createAuthorRequest", PREFIX);
        SOAPElement authorTag = createAuthorRequestTag.addChildElement("author", PREFIX);

        SOAPElement authorIdTag = authorTag.addChildElement("authorId", PREFIX);
        authorIdTag.addTextNode(String.valueOf(author.getAuthorId()));

        SOAPElement authorNameTag = authorTag.addChildElement("authorName", PREFIX);
        SOAPElement firstTag = authorNameTag.addChildElement("first", PREFIX);
        firstTag.addTextNode(author.getAuthorName().getFirst());
        SOAPElement secondTag = authorNameTag.addChildElement("second", PREFIX);
        secondTag.addTextNode(author.getAuthorName().getSecond());

        SOAPElement nationalityTag = authorTag.addChildElement("nationality", PREFIX);
        nationalityTag.addTextNode(author.getNationality());

        SOAPElement birthTag = authorTag.addChildElement("birth", PREFIX);
        SOAPElement dateTag = birthTag.addChildElement("date", PREFIX);
        dateTag.addTextNode(String.valueOf(author.getBirth().getDate()));
        SOAPElement countryTag = birthTag.addChildElement("country", PREFIX);
        countryTag.addTextNode(author.getBirth().getCountry());
        SOAPElement cityTag = birthTag.addChildElement("city", PREFIX);
        cityTag.addTextNode(author.getBirth().getCity());

        SOAPElement authorDescriptionTag = authorTag.addChildElement("authorDescription", PREFIX);
        authorDescriptionTag.addTextNode(author.getAuthorDescription());

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader(HEADER_NAME, SERVER_URI);
        soapMessage.saveChanges();
        return soapMessage;
    }

}
