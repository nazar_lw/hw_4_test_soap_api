package utils;

public interface Constants {

    String SERVER_URI = "libraryService";
    String BASE_URI = "http://localhost:8080/ws";

    String PREFIX = "lib";
    String HEADER_NAME = "SOAPAction";

    String GET_SINGLE_AUTHOR_TAG = "getAuthorRequest";
    String GET_SINGLE_GENRE_TAG = "getGenreRequest";

    String GET_AUTHORS_TAG = "getAuthorsRequest";
    String GET_BOOKS_TAG = "getBooksRequest";
    String GET_GENRES_TAG = "getGenresRequest";

    String DELETE_AUTHOR_TAG = "deleteAuthorRequest";
    String DELETE_BOOK_TAG = "deleteBookRequest";
    String DELETE_GENRE_TAG = "deleteGenreRequest";

    String AUTHOR_ID_TAG = "authorId";
    String BOOK_ID_TAG = "bookId";
    String GENRE_ID_TAG = "genreId";

    String INITIAL_SIZE_OF_COLLECTION = "100";
    String DESC_ORDER = "desc";

    int LOOP_LIMIT = 5000;

    //random data
    String RANDOM_AUTHOR_FIRST_NAME = "SomeAuthorFirstName";
    String RANDOM_AUTHOR_SECOND_NAME = "SomeAuthorSecondName";
    String RANDOM_AUTHOR_NATIONALITY = "SomeAuthor Nationality";
    String RANDOM_AUTHOR_DESCRIPTION = "SomeAuthor Description";
    String RANDOM_AUTHOR_BIRTH_CITY = "Some City";
    String RANDOM_AUTHOR_BIRTH_COUNTRY = "Some Country";
    String RANDOM_AUTHOR_BIRTH_DATE = "1999-10-10";

    String UPDATED_AUTHOR_NATIONALITY = "UPDATED Nationality";
    String UPDATED_AUTHOR_DESCRIPTION = "UPDATED Description";
    String UPDATED_AUTHOR_BIRTH_DATE = "1900-05-05";

    double RANDOM_BOOK_HEIGHT = 11.11;
    double RANDOM_BOOK_WIDTH = 22.22;
    double RANDOM_BOOK_LENGTH = 33.33;
    int RANDOM_BOOK_PAGE_COUNT = 150;
    String RANDOM_BOOK_NAME = "Some Book Name";
    String RANDOM_BOOK_LANGUAGE = "Some Book Language";
    String RANDOM_BOOK_DESCRIPTION = "SomeBookName";
    int RANDOM_BOOK_PUBLICATION_YEAR = 2010;

    String RANDOM_GENRE_NAME = "Some Genre Name";
    String RANDOM_GENRE_DESCRIPTION = "Some Genre Description";

}
