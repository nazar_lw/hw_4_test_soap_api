package parser;

import org.testng.Assert;

import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import static logger.AllureLogger.logToAllureError;

public class BaseParser {

    public BaseParser() {
    }

    public int getHighestId(SOAPMessage soapResponse, String entityIdName) throws SOAPException {

        Assert.assertNull(soapResponse.getSOAPBody().getFault(), "Request failed !");

        int highestId = 0;
        try{
            highestId = Integer.parseInt(soapResponse.
                    getSOAPPart().getEnvelope().getBody().getElementsByTagName("ns2:"+entityIdName).
                    item(0).getTextContent());
        }catch (NumberFormatException e){
            logToAllureError(e.getMessage());
        }
        return highestId;
    }

    public String getResponseOnDelete(SOAPMessage soapResponse) throws SOAPException {

//        Assert.assertNull(soapResponse.getSOAPBody().getFault(), "Request failed !");
        return soapResponse.
                getSOAPPart().getEnvelope().getBody().getElementsByTagName("ns2:status").
                item(0).getTextContent();
    }
}
