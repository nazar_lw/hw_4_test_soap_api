package parser;

import model.genre.Genre;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.util.ArrayList;
import java.util.List;

import static logger.AllureLogger.logToAllureError;
import static utils.Constants.LOOP_LIMIT;

import org.testng.Assert;

public class GenreParser extends BaseParser{


    public GenreParser() {
    }

    public List<Genre> getGenres(SOAPMessage soapResponse) throws SOAPException {

        Assert.assertNull(soapResponse.getSOAPBody().getFault(), "Request failed !");

        SOAPBody soapBody = soapResponse.getSOAPPart().getEnvelope().getBody();
        List<Genre> genres = new ArrayList<>();
        int iterator = 0;
        while (soapBody.getElementsByTagName("ns2:genreId").item(iterator)!=null){

            if(iterator == LOOP_LIMIT){
                return genres;
            }
            Genre genre = new Genre();
            try {
                genre.setGenreId(Integer.parseInt(soapBody.getElementsByTagName("ns2:genreId").
                        item(iterator).getTextContent()));
            } catch (NumberFormatException e) {
                logToAllureError(e.getMessage());
            }
            genre.setGenreName(soapBody.getElementsByTagName("ns2:genreName").
                    item(iterator).getTextContent());
            genre.setGenreDescription(soapBody.getElementsByTagName("ns2:genreDescription").
                    item(iterator).getTextContent());
            genres.add(genre);
            iterator++;
        }
        return genres;
    }

}
