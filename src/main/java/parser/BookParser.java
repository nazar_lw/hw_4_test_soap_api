package parser;

import model.book.Book;
import model.book.nested.Additional;
import model.book.nested.nested.Size;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.util.ArrayList;
import java.util.List;

import static logger.AllureLogger.logToAllureError;
import static utils.Constants.LOOP_LIMIT;

import org.testng.Assert;

public class BookParser {

    public BookParser() {
    }

    public List<Book> getBooks(SOAPMessage soapResponse) throws SOAPException {

        Assert.assertNull(soapResponse.getSOAPBody().getFault(), "Request failed !");

        SOAPBody soapBody = soapResponse.getSOAPPart().getEnvelope().getBody();
        List<Book> books = new ArrayList<>();
        int iterator = 0;
        while (soapBody.getElementsByTagName("ns2:bookId").item(iterator)!= null){

            if(iterator == LOOP_LIMIT){
                return books;
            }
            Book book = new Book();
            try{
                book.setBookId(Integer.parseInt(soapBody.getElementsByTagName("ns2:bookId").
                        item(iterator).getTextContent()));
            }catch (NumberFormatException e){
                logToAllureError(e.getMessage());
            }
            book.setBookName(soapBody.getElementsByTagName("ns2:bookName").
                    item(iterator).getTextContent());
            book.setBookLanguage(soapBody.getElementsByTagName("ns2:bookLanguage").
                    item(iterator).getTextContent());
            book.setBookDescription(soapBody.getElementsByTagName("ns2:bookDescription").
                    item(iterator).getTextContent());
            Additional additional = new Additional();
            try{
                additional.setPageCount(Integer.parseInt(soapBody.getElementsByTagName("ns2:pageCount").
                        item(iterator).getTextContent()));
            }catch (NumberFormatException e){
                logToAllureError(e.getMessage());
            }
            Size size = new Size();
            try{
                size.setHeight(Double.parseDouble(soapBody.getElementsByTagName("ns2:height").
                        item(iterator).getTextContent()));
            }catch (NumberFormatException e){
                logToAllureError(e.getMessage());
            }
            try{
                size.setWidth(Double.parseDouble(soapBody.getElementsByTagName("ns2:width").
                        item(iterator).getTextContent()));
            }catch (NumberFormatException e){
                logToAllureError(e.getMessage());
            }
            try{
                size.setLength(Double.parseDouble(soapBody.getElementsByTagName("ns2:length").
                        item(iterator).getTextContent()));
            }catch (NumberFormatException e){
                logToAllureError(e.getMessage());
            }
            additional.setSize(size);
            book.setAdditional(additional);
            try{
                book.setPublicationYear(Integer.parseInt(soapBody.getElementsByTagName("ns2:publicationYear").
                        item(iterator).getTextContent()));
            }catch (NumberFormatException e){
                logToAllureError(e.getMessage());
            }
            books.add(book);
            iterator++;
        }
        return books;
    }
}
