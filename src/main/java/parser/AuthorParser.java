package parser;

import model.author.Author;
import model.author.nested.Birth;
import model.author.nested.Name;

import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static logger.AllureLogger.logToAllureError;
import static utils.Constants.LOOP_LIMIT;

import org.testng.Assert;

public class AuthorParser {

    public AuthorParser() {
    }

    public List<Author> getAuthors(SOAPMessage soapResponse) throws SOAPException{

        Assert.assertNull(soapResponse.getSOAPBody().getFault(), "Request failed !");

        SOAPBody soapBody = soapResponse.getSOAPPart().getEnvelope().getBody();
        List<Author> authors = new ArrayList<>();
        int iterator = 0;
        while (soapBody.getElementsByTagName("ns2:authorId").item(iterator)!=null){

            if(iterator == LOOP_LIMIT){
                return authors;
            }
            Author author = new Author();
            try{
                author.setAuthorId(Integer.parseInt(soapBody.getElementsByTagName("ns2:authorId").
                        item(iterator).getTextContent()));
            }catch (NumberFormatException e){
                logToAllureError(e.getMessage());
            }
            author.setNationality(soapBody.getElementsByTagName("ns2:nationality").
                    item(iterator).getTextContent());
            author.setAuthorDescription(soapBody.getElementsByTagName("ns2:authorDescription").
                    item(iterator).getTextContent());
            Birth birth = new Birth();
            birth.setDate(LocalDate.parse(soapBody.getElementsByTagName("ns2:date").
                    item(iterator).getTextContent()));
            birth.setCity(soapBody.getElementsByTagName("ns2:city").
                    item(iterator).getTextContent());
            birth.setCountry(soapBody.getElementsByTagName("ns2:country").
                    item(iterator).getTextContent());
            author.setBirth(birth);
            Name name = new Name();
            name.setFirst(soapBody.getElementsByTagName("ns2:first").
                    item(iterator).getTextContent());
            name.setSecond(soapBody.getElementsByTagName("ns2:second").
                    item(iterator).getTextContent());
            author.setAuthorName(name);
            authors.add(author);
            iterator++;
        }
        return authors;
    }

}
