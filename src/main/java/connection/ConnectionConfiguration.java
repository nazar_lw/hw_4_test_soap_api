package connection;


import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPException;

import static logger.AllureLogger.*;

public class ConnectionConfiguration {

    private static SOAPConnection connection = null;

    public static SOAPConnection getSoapConnection() {
        if (connection == null) {
            try {
                SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
                connection = soapConnectionFactory.createConnection();
                logToAllureInfo("Creating SOAPconnection ");
            } catch (SOAPException e) {
                logToAllureError(e.getMessage());
            }
        }
        return connection;
    }
}
