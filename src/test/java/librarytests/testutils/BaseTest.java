package librarytests.testutils;

import io.qameta.allure.Step;
import model.author.Author;
import model.book.Book;
import model.genre.Genre;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Listeners;
import parser.AuthorParser;
import parser.BaseParser;
import parser.BookParser;
import parser.GenreParser;
import requestgenerator.AuthorRequestGenerator;
import requestgenerator.BaseRequestGenerator;
import requestgenerator.BookRequestGenerator;
import requestgenerator.GenreRequestGenerator;
import utils.RandomDataProvider;

import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;

import static connection.ConnectionConfiguration.getSoapConnection;
import static logger.AllureLogger.*;
import static utils.Constants.*;

@Listeners({TestListener.class})
public abstract class BaseTest {

    protected RandomDataProvider randomDataProvider;
    protected SOAPConnection connection;
    protected BaseRequestGenerator baseRequestGenerator;
    protected BaseParser baseParser;
    protected AuthorRequestGenerator authorRequestGenerator;
    protected AuthorParser authorParser;
    protected BookRequestGenerator bookRequestGenerator;
    protected BookParser bookParser;
    protected GenreRequestGenerator genreRequestGenerator;
    protected GenreParser genreParser;

    @BeforeClass
    public void reinitialize() {
        connection = getSoapConnection();
        baseParser = new BaseParser();
        baseRequestGenerator = new BaseRequestGenerator();
        authorParser = new AuthorParser();
        authorRequestGenerator = new AuthorRequestGenerator();
        bookParser = new BookParser();
        bookRequestGenerator = new BookRequestGenerator();
        genreParser = new GenreParser();
        genreRequestGenerator = new GenreRequestGenerator();
        randomDataProvider = new RandomDataProvider();
        logToAllureWarn("BeforeMethod: Initialize fields");
    }

    @AfterSuite
    public void cleanUp() {
        try {
            connection.close();
        } catch (SOAPException e) {
            logToAllureError(e.getMessage());
        }
        logToAllureWarn("AfterSuite: Close connection");
    }

    @Step("Get the highest id from the List of entities")
    protected int getHighestId(String idTag, String requestTag) throws SOAPException {
        SOAPMessage response  = connection.
                call(baseRequestGenerator.getAllByOrder(DESC_ORDER, requestTag), BASE_URI);
        logToAllureDebug("Getting all entities by "+ requestTag +" in "+ DESC_ORDER +"ending order");
        return baseParser.getHighestId(response, idTag);
    }

    @Step("Save to the DB 3 random entities of Author, Genre, Book and get their id ")
    protected int[] saveRandomEntities() throws Exception {
        int incrementedAuthorId = getHighestId(AUTHOR_ID_TAG, GET_AUTHORS_TAG) +1;
        Author author = randomDataProvider.generateRandomAuthor(incrementedAuthorId);
        SOAPMessage responseSaveAuthor = connection.
                call(authorRequestGenerator.saveAuthor(author), BASE_URI);
        Author authorOnResponseOnSave = authorParser.getAuthors(responseSaveAuthor).get(0);
        Assert.assertEquals(authorOnResponseOnSave, author, "Authors do not match");
        logToAllureDebug("Saving a Random Author with id: " + incrementedAuthorId);

        int incrementedGenreId = getHighestId(GENRE_ID_TAG, GET_GENRES_TAG) +1;
        Genre genre = randomDataProvider.generateRandomGenre(incrementedGenreId);
        SOAPMessage responseOnSaveGenre = connection.
                call(genreRequestGenerator.saveGenre(genre), BASE_URI);
        Genre genreOnResponseOnSave = genreParser.getGenres(responseOnSaveGenre).get(0);
        Assert.assertEquals(genreOnResponseOnSave, genre, "Genres do not match");
        logToAllureDebug("Saving a Random Genre with id: " + incrementedGenreId);

        int incrementedBookId = getHighestId(BOOK_ID_TAG, GET_BOOKS_TAG) +1;
        Book book = randomDataProvider.generateRandomBook(incrementedBookId);
        SOAPMessage responseSaveBook = connection.
                call(bookRequestGenerator.saveBook(book, incrementedAuthorId, incrementedGenreId), BASE_URI);
        Book bookOnResponseOnSave = bookParser.getBooks(responseSaveBook).get(0);
        Assert.assertEquals(bookOnResponseOnSave, book, "Books do not match");
        logToAllureDebug("Saving a Random Book with id: " + incrementedBookId);

        return new int[]{incrementedAuthorId, incrementedGenreId, incrementedBookId};
    }


}
