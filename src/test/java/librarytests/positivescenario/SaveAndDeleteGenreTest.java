package librarytests.positivescenario;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import librarytests.testutils.BaseTest;
import model.genre.Genre;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPMessage;

import static logger.AllureLogger.logToAllureInfo;
import static utils.Constants.*;

public class SaveAndDeleteGenreTest extends BaseTest {

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Verify creating and deleting of a Genre")
    @Step("Verify creating and deleting of a Genre")
    public void testSaveAndDeleteGenre() throws Exception {

        int incrementedGenreId = getHighestId(GENRE_ID_TAG, GET_GENRES_TAG) +1;
        Genre genre = randomDataProvider.generateRandomGenre(incrementedGenreId);
        logToAllureInfo("Creating a Genre with id: " + incrementedGenreId);

        SOAPMessage responseOnSaveGenre = connection.
                call(genreRequestGenerator.saveGenre(genre), BASE_URI);
        Genre genreOnResponseOnSave = genreParser.getGenres(responseOnSaveGenre).get(0);
        Assert.assertEquals(genreOnResponseOnSave, genre, "Genres do not match");
        logToAllureInfo("Saving a Genre with id: " + incrementedGenreId);

        SOAPMessage responseOnDelete = connection.call(genreRequestGenerator.
                deleteEntity(String.valueOf(incrementedGenreId), DELETE_GENRE_TAG, GENRE_ID_TAG), BASE_URI);
        String statusOnDelete = baseParser.getResponseOnDelete(responseOnDelete);
        Assert.assertEquals(statusOnDelete, "Successfully deleted genre "+incrementedGenreId, "Delete failed");
        logToAllureInfo("Deleting a Genre with id: " + incrementedGenreId);
    }
}
