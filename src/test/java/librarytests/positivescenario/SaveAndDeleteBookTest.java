package librarytests.positivescenario;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import librarytests.testutils.BaseTest;
import model.book.Book;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPMessage;

import static logger.AllureLogger.logToAllureInfo;
import static utils.Constants.*;

public class SaveAndDeleteBookTest extends BaseTest {

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Verify creating and deleting of a Book")
    @Step("Verify creating and deleting of a Book")
    public void testSaveAndDeleteBook() throws Exception {

        int incrementedBookId = getHighestId(BOOK_ID_TAG, GET_BOOKS_TAG) +1;
        int realAuthorId = getHighestId(AUTHOR_ID_TAG, GET_AUTHORS_TAG);
        int realGenreId = getHighestId(GENRE_ID_TAG, GET_GENRES_TAG);
        Book book = randomDataProvider.generateRandomBook(incrementedBookId);
        logToAllureInfo("Creating a Book with id: " + incrementedBookId);

        SOAPMessage responseSaveBook = connection.
                call(bookRequestGenerator.saveBook(book, realAuthorId, realGenreId), BASE_URI);
        Book bookOnResponseOnSave = bookParser.getBooks(responseSaveBook).get(0);
        Assert.assertEquals(bookOnResponseOnSave, book, "Books do not match");
        logToAllureInfo("Saving a Book with id: " + incrementedBookId);

        SOAPMessage responseOnDelete = connection.call(bookRequestGenerator.
                deleteEntity(String.valueOf(incrementedBookId), DELETE_BOOK_TAG, BOOK_ID_TAG), BASE_URI);
        String statusOnDelete = baseParser.getResponseOnDelete(responseOnDelete);
        Assert.assertEquals(statusOnDelete, "Successfully deleted book "+incrementedBookId, "Delete failed");
        logToAllureInfo("Deleting a Book with id: " + incrementedBookId);
    }
}
