package librarytests.positivescenario;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import librarytests.testutils.BaseTest;
import model.book.Book;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPMessage;

import static logger.AllureLogger.logToAllureInfo;
import static utils.Constants.*;

public class GetBooksByGenreTest extends BaseTest {

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Verify getting a Book by its Genre having Author, Genre and Book saved in the DB")
    @Step("Verify getting a Book by its Genre having Author, Genre and Book saved in the DB")
    public void testGetBookByGenre() throws Exception {

        int[] idArray = saveRandomEntities();

        SOAPMessage getBooksByGenre = connection.
                        call(bookRequestGenerator.getAllBooksByGenre(idArray[1]), BASE_URI);
        Book bookReceivedByGenreId = bookParser.getBooks(getBooksByGenre).get(0);
        Assert.assertEquals(bookReceivedByGenreId.getBookId(), idArray[2], "Id do not match");
        logToAllureInfo("Verifying creating a Book and getting it from the server by genreId");

        SOAPMessage responseOnDeleteRandomBook = connection.call(bookRequestGenerator.
                deleteEntity(String.valueOf(idArray[2]), DELETE_BOOK_TAG, BOOK_ID_TAG), BASE_URI);
        String statusOnDeleteRandomBook = baseParser.getResponseOnDelete(responseOnDeleteRandomBook);
        Assert.assertEquals(statusOnDeleteRandomBook, "Successfully deleted book "+idArray[2], "Delete failed");
        logToAllureInfo("Deleting a Book with id: " + idArray[2]);

        SOAPMessage responseOnDeleteRandomAuthor = connection.call(authorRequestGenerator.
                deleteEntity(String.valueOf(idArray[0]), DELETE_AUTHOR_TAG, AUTHOR_ID_TAG), BASE_URI);
        String statusOnDeleteRandomAuthor = baseParser.getResponseOnDelete(responseOnDeleteRandomAuthor);
        Assert.assertEquals(statusOnDeleteRandomAuthor, "Successfully deleted author "+idArray[0], "Delete failed");
        logToAllureInfo("Deleting an Author with id: " + idArray[0]);

        SOAPMessage responseOnDeleteRandomGenre = connection.call(genreRequestGenerator.
                deleteEntity(String.valueOf(idArray[1]), DELETE_GENRE_TAG, GENRE_ID_TAG), BASE_URI);
        String statusOnDeleteRandomGenre = baseParser.getResponseOnDelete(responseOnDeleteRandomGenre);
        Assert.assertEquals(statusOnDeleteRandomGenre, "Successfully deleted genre "+idArray[1], "Delete failed");
        logToAllureInfo("Deleting a Genre with id: " + idArray[1]);

    }
}
