package librarytests.positivescenario;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import librarytests.testutils.BaseTest;
import model.author.Author;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPMessage;

import static logger.AllureLogger.logToAllureInfo;
import static utils.Constants.*;

public class SaveAndDeleteAuthorTest extends BaseTest {

    @Test
    @Severity(SeverityLevel.NORMAL)
    @Description("Verify creating and deleting of an Author")
    @Step("Verify creating and deleting of an Author")
    public void testSaveAndDeleteAuthor() throws Exception {

        int incrementedAuthorId = getHighestId(AUTHOR_ID_TAG, GET_AUTHORS_TAG) +1;
        Author author = randomDataProvider.generateRandomAuthor(incrementedAuthorId);
        logToAllureInfo("Creating an Author with id: " + incrementedAuthorId);

        SOAPMessage responseSaveAuthor = connection.
                call(authorRequestGenerator.saveAuthor(author), BASE_URI);
        Author authorOnResponseOnSave = authorParser.getAuthors(responseSaveAuthor).get(0);
        Assert.assertEquals(authorOnResponseOnSave, author, "Authors do not match");
        logToAllureInfo("Saving an Author with id: " + incrementedAuthorId);

        SOAPMessage responseOnDelete = connection.call(authorRequestGenerator.
                deleteEntity(String.valueOf(incrementedAuthorId), DELETE_AUTHOR_TAG, AUTHOR_ID_TAG), BASE_URI);
        String statusOnDelete = baseParser.getResponseOnDelete(responseOnDelete);
        Assert.assertEquals(statusOnDelete, "Successfully deleted author "+incrementedAuthorId, "Delete failed");
        logToAllureInfo("Deleting an Author with id: " + incrementedAuthorId);
    }
}
