package librarytests.negativescenario;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import librarytests.testutils.BaseTest;
import model.genre.Genre;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPMessage;
import java.util.List;

import static logger.AllureLogger.logToAllureDebug;
import static logger.AllureLogger.logToAllureInfo;
import static utils.Constants.*;

public class SaveGenreWithExistedNameTest extends BaseTest {

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify saving a Genre with existent name")
    @Step("Verify saving a Genre with existent name")
    public void saveGenreWithExistedName() throws Exception {

        SOAPMessage responseGetAll = connection.
                call(baseRequestGenerator.getAllByOrder(DESC_ORDER, GET_GENRES_TAG), BASE_URI);
        List<Genre> genres = genreParser.getGenres(responseGetAll);
        logToAllureDebug("Getting List of Genre with size: "+genres.size());
        Genre existedGenre = genres.get(genres.size()/2);
        int incrementedGenreId = getHighestId(GENRE_ID_TAG, GET_GENRES_TAG) + 1;
        existedGenre.setGenreId(incrementedGenreId);
        logToAllureInfo("Setting nonexistent genreId to existent Genre");
        SOAPMessage responseOnSaveGenre = connection.
                call(genreRequestGenerator.saveGenre(existedGenre), BASE_URI);

        Assert.assertNotNull(responseOnSaveGenre.getSOAPBody().getFault(), "Request successful !");
        logToAllureInfo("Trying to save existed Genre with nonexistent genreId");
        Assert.assertEquals(responseOnSaveGenre.getSOAPBody().getFault().getFaultString(),
                "Genre with name "+existedGenre.getGenreName()+" already exists");
        logToAllureInfo("Falling to save - genre with "+existedGenre.getGenreName()+" already exists");
    }
}
