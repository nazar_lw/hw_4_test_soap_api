package librarytests.negativescenario;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import librarytests.testutils.BaseTest;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPMessage;

import static logger.AllureLogger.logToAllureInfo;
import static utils.Constants.*;

public class GetNonexistentGenreTest extends BaseTest {

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify getting a nonexistent Genre")
    @Step("Verify getting a nonexistent Genre")
    public void testGetNonexistentGenre() throws Exception {

        int incrementedGenreId = getHighestId(GENRE_ID_TAG, GET_GENRES_TAG) + 1;
        logToAllureInfo("Getting the id of nonexistent Genre: "+ 1);
        SOAPMessage responseGetGenre = connection.
                call(baseRequestGenerator.getSingleEntity(GET_SINGLE_GENRE_TAG,
                        GENRE_ID_TAG, incrementedGenreId), BASE_URI);

        Assert.assertNotNull(responseGetGenre.getSOAPBody().getFault(), "Request successful !");
        logToAllureInfo("Getting a nonexistent Genre from the server DB ");
        Assert.assertEquals(responseGetGenre.getSOAPBody().getFault().getFaultString(),
                "Genre with id "+incrementedGenreId+" not found", "Genre exists in the DB");
        logToAllureInfo("Verifying the possibility of getting a nonexistent Genre");
    }
}
