package librarytests.negativescenario;

import io.qameta.allure.Description;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import librarytests.testutils.BaseTest;
import model.author.Author;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.xml.soap.SOAPMessage;

import static logger.AllureLogger.logToAllureInfo;
import static utils.Constants.*;

public class SaveExistedAuthorTest extends BaseTest {

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("Verify saving an Author with existent authorId")
    @Step("Verify saving an Author with existent authorId")
    public void testSaveExistedAuthor() throws Exception {

        int realAuthorId = getHighestId(AUTHOR_ID_TAG, GET_AUTHORS_TAG);
        logToAllureInfo("Getting the id of a real author: "+realAuthorId);
        SOAPMessage responseGetAuthor = connection.
                call(baseRequestGenerator.getSingleEntity(GET_SINGLE_AUTHOR_TAG,
                        AUTHOR_ID_TAG, realAuthorId), BASE_URI);
        Author existedAuthor = authorParser.getAuthors(responseGetAuthor).get(0);
        logToAllureInfo("Getting real Author from the server DB - "+ existedAuthor.getAuthorName());
        SOAPMessage responseOnSaveSameAuthor = connection.
                call(authorRequestGenerator.saveAuthor(existedAuthor), BASE_URI);

        Assert.assertNotNull(responseOnSaveSameAuthor.getSOAPBody().getFault(), "Request successful !");
        Assert.assertEquals(responseOnSaveSameAuthor.getSOAPBody().getFault().getFaultString(),
                "Author with id "+realAuthorId+" already exists", "Author has been saved");
        logToAllureInfo("Verifying the possibility of saving an Author that already exists");
    }
}
